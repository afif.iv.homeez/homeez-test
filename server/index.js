const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const apiRoute = require("./routes/api/index.js");

const RequestOrigin = process.env.REQ_ORIGIN || "http://localhost:3000";
const ServerPort = process.env.PORT || 5000;
const ServerHost = '0.0.0.0';

/**
 * Express config
 */
const app = express();
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

// Add middleware to serve reactjs build folder
app.use(express.static(path.join(__dirname, "../client", "build")));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", RequestOrigin);
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS");
  next();
});

/**
 * API Routing Config
 */
app.use("/api", apiRoute);
app.get("/api/*", function (req, res) {
  res.status(400).send(`<strong>400</strong> That's an error.`);
});

app.get("*", function (req, res) {
  res.sendFile(path.join(__dirname, "../client", "build/index.html"));
});

// Start express at port 5000
app.listen(ServerPort, ServerHost, () => {
  console.log(`Server started on port ${ServerPort}`);
});
