exports.up = function (knex) {
  return knex.schema.createTable("Quotation", function (table) {
    table.increments('Q_ID');
    table.string("Quotation_Info").notNullable();
    table.string("Quotation_Valid").defaultTo("TRUE");
    table.timestamp("created_at").defaultTo(knex.fn.now());
    table.timestamp("updated_at").defaultTo(knex.fn.now());
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("Quotation");
};