const express = require('express');
const router = express.Router();
const quotation = require('./quotation');

router.use('/quotation', quotation);

module.exports = router;