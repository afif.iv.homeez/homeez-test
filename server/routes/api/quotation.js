const express = require("express");
const router = express.Router();
const db = require("../../database");

const TABLE_NAME = "Quotation";

// Get all quotations
router.get("/", function (req, res) {
  db.select()
    .from(TABLE_NAME)
    .orderBy("Q_ID")
    .then(function (data) {
      res.send(data);
    });
});

// Insert new quotation
router.post("/", function (req, res) {
  db.insert(req.body)
    .returning("*")
    .into(TABLE_NAME)
    .then(function (data) {
      res.send(data);
    });
});

// Update quotation valid column
router.put("/:id", function (req, res) {
  db(TABLE_NAME)
    .where({ Q_ID: req.params.id })
    .update({
      Quotation_Valid: req.body.valid || "TRUE",
    })
    .returning("*")
    .then(function (data) {
      res.send(data);
    });
});

module.exports = router;
