import api from "./config";

const quotation = {
    /**
     * Get list of quotations
     * @return Array<quotations>
     */
    getQuotationList: () => api.get("/quotation"),

    /**
     * Add new quotation
     * @param {Object} Quotation_Info
     * @return {Object}
     */
    addQuotation: (param) => api.post("/quotation", param),

    /**
     * Update quotation valid
     * @param {String} id
     * @param {String} "TRUE||FALSE"
     * @return {Object}
     */
    updateQuotationValid: (id, valid) => api.put(`/quotation/${id}`, { valid })
}

export { quotation };