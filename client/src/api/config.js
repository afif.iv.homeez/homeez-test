const BASE_URL = `${window.location.origin}/api`;
const axios = require("axios");

const api = axios.create({
  baseURL: BASE_URL,
  timeout: 5000,
});

export default api;