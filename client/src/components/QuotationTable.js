import React from "react";
import { Table, Toggle } from "rsuite";

const { Column, HeaderCell, Cell } = Table;

const QuotationTable = (props) => (
  <Table
    loading={props.loading}
    height={400}
    data={props.data}
    onRowClick={(data) => {
      // console.log(data);
    }}
  >
    <Column width={500}>
      <HeaderCell>Description</HeaderCell>
      <Cell dataKey="Quotation_Info" />
    </Column>

    <Column width={120} fixed="right">
      <HeaderCell>Valid</HeaderCell>

      <Cell>
        {(rowData) => {
          return (
            <Toggle
              checked={rowData.Quotation_Valid === "TRUE"}
              onChange={(valid) =>
                props.onChange ? props.onChange(rowData, valid) : null
              }
            />
          );
        }}
      </Cell>
    </Column>
  </Table>
);

export default QuotationTable;
