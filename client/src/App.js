import React from "react";
import "./App.css";
import "rsuite/dist/styles/rsuite-default.css";

import { Container, Header, Content, Footer, Navbar, Nav, Icon } from "rsuite";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
} from "react-router-dom";

import { Home, Quotations } from "./screens";

function App() {
  return (
    <Router>
      <Container>
        <Header>
          <Navbar appearance="inverse">
            <Navbar.Header>
              <span className="navbar-brand logo">Homeez Test</span>
            </Navbar.Header>
            <Navbar.Body>
              <Nav>
                <Nav.Item
                  icon={<Icon icon="home" />}
                  componentClass={NavLink}
                  to="/"
                >
                  Home
                </Nav.Item>
                <Nav.Item componentClass={NavLink} to="/quotations">
                  Quotations
                </Nav.Item>
              </Nav>
            </Navbar.Body>
          </Navbar>
        </Header>
        <Content className="container">
          <Switch>
            <Route path="/quotations">
              <Quotations />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Content>
        <Footer className="footer">
          <span>&copy; 2020 by afif</span>
        </Footer>
      </Container>
    </Router>
  );
}

export default App;