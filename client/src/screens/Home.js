import React, { useState } from "react";
import {
  FlexboxGrid,
  Panel,
  Form,
  FormGroup,
  ControlLabel,
  FormControl,
  ButtonToolbar,
  Button,
  Col,
  Notification,
} from "rsuite";
import { Link } from "react-router-dom";
import { quotation } from "../api";

export default function Home() {
  // States
  const [loading, setLoading] = useState(false);
  const [input, setInput] = useState("");

  // Trigger success notification
  const notitfySuccess = (Quotation_Info) => {
    Notification["success"]({
      title: "New Quotation Added",
      description: `"${Quotation_Info}" was added to the Quotation List`,
    });
  };

  // Add new quotation
  const addQuotation = async () => {
    if (input === "") {
      return false;
    }

    try {
      setLoading(true);
      await quotation.addQuotation({ Quotation_Info: input });
      notitfySuccess(input);
    } catch (err) {
      alert("Error while requesting: " + err.message);
    } finally {
      setLoading(false);
    }
  };

  return (
    <FlexboxGrid justify="center">
      <FlexboxGrid.Item componentClass={Col} colspan={24} sm={12} md={8}>
        <Panel header={<h3>New Quotation</h3>} bordered>
          <Form fluid>
            <FormGroup>
              <ControlLabel>Description</ControlLabel>
              <FormControl
                name="info"
                placeholder="Enter quotation description"
                value={input}
                onChange={(input) => setInput(input)}
              />
            </FormGroup>
            <FormGroup>
              <ButtonToolbar>
                <Button
                  appearance="primary"
                  block
                  loading={loading}
                  onClick={addQuotation}
                >
                  Add New Quotation
                </Button>
              </ButtonToolbar>
            </FormGroup>
          </Form>
        </Panel>
        <ButtonToolbar>
          <Button
            appearance="link"
            componentClass={Link}
            to="/quotations"
            block
          >
            Quotation List
          </Button>
        </ButtonToolbar>
      </FlexboxGrid.Item>
    </FlexboxGrid>
  );
}
