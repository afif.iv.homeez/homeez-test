import React, { useState, useEffect, useCallback } from "react";
import { FlexboxGrid, Panel, Col } from "rsuite";

import QuotationTable from "../components/QuotationTable";
import { quotation } from "../api";

function Quotations() {
  // States
  const [loading, setLoading] = useState(false);
  const [quoteList, setQuoteList] = useState([]);

  // Retrieve quotations list
  const getQuotations = useCallback(async () => {
    let response = await quotation.getQuotationList();
    setQuoteList(response.data || []);
  }, []);

  // Update quotation valid
  const updateValid = async (data, valid) => {
    try {
      setLoading(true);
      let response = await quotation.updateQuotationValid(
        data.Q_ID,
        valid.toString().toUpperCase()
      );

      // Updated back to the list
      const updated = response.data[0];
      const updatedItems = quoteList.map(el => el.Q_ID === updated.Q_ID ? updated : el);
      setQuoteList(updatedItems);

    } catch (err) {
      alert("Error while requesting: " + err.message);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getQuotations();
  }, [getQuotations]);

  // Render
  return (
    <FlexboxGrid justify="center">
      <FlexboxGrid.Item componentClass={Col} colspan={24} md={12}>
        <Panel header={<h3>Quotation List</h3>} bordered>
          <QuotationTable
            loading={loading}
            data={quoteList}
            onChange={updateValid}
          />
        </Panel>
      </FlexboxGrid.Item>
    </FlexboxGrid>
  );
}

export default Quotations;
