# Homeez Test

Fullstack codebase sample implementing NodeJS and ReactJS using the same server instance with CI/CD integration for the deployment.

***

### Getting Started

#### 1) Clone & Install Dependencies

- 1.1) Clone this repository
- 1.2) `cd <folder-name>` - cd into your newly cloned project directory.
- 1.3) Install NPM packages by running `yarn`

#### 2) Database Migration (Knex)

- 2.1) `cd server` - cd into server directory
- 2.2) Make sure Knex globally installed by `npm install -g knex`
- 2.3) Configure database accordingly in `knexfile.json`
- 2.4) Run `knex migrate:latest` to migrate database structure.
  
#### 3) Start your app (Client-Side)

- 3.1) Run `yarn dev` to start ReactJS development.
- 3.2) The application will be running on localhost:3000

#### 4) Start your app (Server-Side)

- 4.1) Run `yarn dev-server` to start NodeJS development.
- 4.2) The application will be running on localhost:5000 / localhost:ENV

##### NOTES
> This script will **build** Client-Side of ReactJS and serve it on root path, API development should be done on "/api" path